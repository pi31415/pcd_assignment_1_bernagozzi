package pcd.ass01.ex2;
import java.util.concurrent.Semaphore;

/**
 * Created by ste on 27/03/2017.
 */
public class Main {
    public static Counter counter;
    public static Semaphore semaphorePinger;
    public static Semaphore semaphorePonger;
    public static Semaphore semaphoreViewer;
    public static Semaphore semaphoreStopViewer;
    public static Pinger pinger;
    public static Ponger ponger;
    public static Viewer viewer;
    public static MyView view;
    public static boolean isNotStopped;

    public static void main(String[] args) throws Exception {
        isNotStopped = true;
        semaphoreStopViewer = new Semaphore(1);
        semaphorePinger = new Semaphore(1);
        semaphorePonger = new Semaphore(1);
        semaphoreViewer = new Semaphore(1);
        counter = new Counter();
        pinger = new Pinger();
        ponger = new Ponger();
        viewer = new Viewer();
        MyController controller = new MyController();


        try {
            semaphoreViewer.acquire();
            semaphorePonger.acquire();
        } catch (Exception e) {

        }
        MyView view = new MyView(controller);
        view.setVisible(true);
    }
}
