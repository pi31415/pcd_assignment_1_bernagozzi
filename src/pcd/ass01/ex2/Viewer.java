package pcd.ass01.ex2;

/**
 * Created by ste on 27/03/2017.
 */
public class Viewer extends Thread{
    private boolean isNotStopped;
    private boolean oneThreadAlredyStopped;
    public Viewer() {
        isNotStopped = true;
        oneThreadAlredyStopped = false;
    }

    public void run() {
        while(isNotStopped) {
            try {
                Main.semaphoreViewer.acquire();
                if (isNotStopped) {
	                System.out.println(Main.counter.getValue());
	                if (Main.counter.getValue() % 2 == 1) {
	                    Main.semaphorePonger.release();
	                } else {
	                    Main.semaphorePinger.release();
	                }
                }
            } catch (Exception e) {

            }
        }
        System.out.println("Goodbye.");
    }
    /*
    public void joinPinger() {
    	try {
    		Main.pinger.join();
    	} catch (Exception e) {}
    }

    public void joinPonger() {
    	try {
    		Main.ponger.join();
    	} catch (Exception e) {}
    }*/

    public void stopThread () {
        if (!oneThreadAlredyStopped) {
            oneThreadAlredyStopped = true;
            Main.semaphoreStopViewer.release();
        } else {
            isNotStopped = false;
        }
    }
}
