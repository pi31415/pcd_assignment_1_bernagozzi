package pcd.ass01.ex2;

/**
 * Created by ste on 27/03/2017.
 */
public class Ponger extends Thread{

    public void run() {
        try {
            while (Main.isNotStopped) {
                Main.semaphorePonger.acquire();
                Main.counter.increment();
                System.out.println("pong!");
                Main.semaphoreViewer.release();
            }
            Main.semaphoreStopViewer.acquire();
            Main.viewer.stopThread();
            //Main.viewer.joinPonger();
            Main.semaphoreViewer.release();
            System.out.println("Goodbye.");
        } catch (Exception e) {}
    }
}
