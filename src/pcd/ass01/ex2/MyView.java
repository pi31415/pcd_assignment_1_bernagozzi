package pcd.ass01.ex2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

class MyView extends JFrame implements ActionListener {
	private MyController controller;
	
	public MyView(MyController contr) {
		super("My View");

		controller = contr;
		setSize(400, 60);
		setResizable(false);
		
		JButton button1 = new JButton("Start");
		button1.addActionListener(this);

		JButton button2 = new JButton("Stop");
		button2.addActionListener(this);
		
		JPanel panel = new JPanel();
		panel.add(button1);		
		panel.add(button2);	

		setLayout(new BorderLayout());
	    add(panel,BorderLayout.NORTH);
	    		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent ev) {
				System.exit(-1);
			}
		});
	}
	
	public void actionPerformed(ActionEvent ev) {
		try {
			controller.processEvent(ev.getActionCommand());
		} catch (Exception ex) {
		}
	}
}
