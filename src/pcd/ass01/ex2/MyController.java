package pcd.ass01.ex2;


public class MyController {

	public MyController(){
	}

	public void processEvent(String event) {
		try {
			if(event.equals("Stop")) {
				Main.isNotStopped = false;
			} else {
				Main.pinger.start();
				Main.ponger.start();
				Main.viewer.start();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
