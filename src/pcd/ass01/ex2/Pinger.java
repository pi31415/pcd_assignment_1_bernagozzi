package pcd.ass01.ex2;

/**
 * Created by ste on 27/03/2017.
 */
public class Pinger extends Thread{
    public void run() {
        try {
            while(Main.isNotStopped) {
                Main.semaphorePinger.acquire();
                Main.counter.increment();
                System.out.println("ping!");
                Main.semaphoreViewer.release();
            }
            Main.semaphoreStopViewer.acquire();
            Main.viewer.stopThread();
            //Main.viewer.joinPinger();
            Main.semaphoreViewer.release();
            System.out.println("Goodbye.");
        } catch (Exception e) {}
    }
}
