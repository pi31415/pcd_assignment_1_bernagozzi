package pcd.ass01.ex2;

/**
 * Created by ste on 27/03/2017.
 */
public class Counter {
    private int value;

    public Counter() {
        value = 0;
    }

    public void increment() {
        value++;
    }

    public int getValue() {
        return value;
    }
}
