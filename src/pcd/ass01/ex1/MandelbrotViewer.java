package pcd.ass01.ex1;

import java.lang.reflect.MalformedParameterizedTypeException;

/**
 * Simple Mandelbrot Set Viewer 
 *		 
 * @author aricci
 *
 */
public class MandelbrotViewer {
    public static void callComplex(int width, int height, int nThread, Complex c, double rad, int nIter) throws MalformedParameterizedTypeException, MandelbrotSetImageConcurException {


		/* creating the set */
        MandelbrotSetImage set = new MandelbrotSetImageConcurImpl(width,height, c, rad);

        System.out.println("Computing...");
        StopWatch cron = new StopWatch();
        cron.start();

		/* computing the image */
        set.compute(nIter, nThread);
        cron.stop();
        System.out.println("done - "+cron.getTime()+" ms");

		/* showing the image */
        MandelbrotView view = new MandelbrotView(set,1200,600);
        view.setVisible(true);


    }
	public static void main(String[] args) throws Exception {
        int nThread = 8;
		/* size of the mandelbrot set in pixel */
        int width = 4000;
        int height = 4000;

		/* number of iteration */
        int nIter = 500;

		/* region to be represented: center and radius */
        Complex c0 = new Complex(0,0);
        double rad0 = 2;

        Complex c1 = new Complex(-0.75,0.1);
        double rad1 = 0.02;

        Complex c2 = new Complex(0.7485,0.0505);
        double rad2 = 0.000002;

        Complex c3 = new Complex(0.254,0);
        double rad3 = 0.001;

        Complex c4 = new Complex(0.254036,0.000409);
        double rad4 = 0.001;
//        callComplex(width, height, 1, c0, rad0, nIter);
//        callComplex(width, height, 2, c0, rad0, nIter);
//        callComplex(width, height, 4, c0, rad0, nIter);
//        callComplex(width, height, 8, c0, rad0, nIter);
//        callComplex(width, height, 16, c0, rad0, nIter);

//        callComplex(width, height, 1, c1, rad1, nIter);
//        callComplex(width, height, 2, c1, rad1, nIter);
//        callComplex(width, height, 4, c1, rad1, nIter);
//        callComplex(width, height, 8, c1, rad1, nIter);
//        callComplex(width, height, 16, c1, rad1, nIter);

//        callComplex(width, height, 1, c2, rad2, nIter);
//        callComplex(width, height, 2, c2, rad2, nIter);
//        callComplex(width, height, 4, c2, rad2, nIter);
//        callComplex(width, height, 8, c2, rad2, nIter);
//        callComplex(width, height, 16, c2, rad2, nIter);

//        callComplex(width, height, 1, c3, rad3, nIter);
//        callComplex(width, height, 2, c3, rad3, nIter);
//        callComplex(width, height, 4, c3, rad3, nIter);
//        callComplex(width, height, 8, c3, rad3, nIter);
//        callComplex(width, height, 16, c3, rad3, nIter);

//        callComplex(width, height, 1, c4, rad4, nIter);
//        callComplex(width, height, 2, c4, rad4, nIter);
//        callComplex(width, height, 4, c4, rad4, nIter);
//        callComplex(width, height, 8, c4, rad4, nIter);
        callComplex(width, height, 16, c4, rad4, nIter);

    }

}
