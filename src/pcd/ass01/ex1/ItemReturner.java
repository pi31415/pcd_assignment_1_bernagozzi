package pcd.ass01.ex1;

/**
 * Created by ste on 23/03/2017.
 */
public class ItemReturner {
    private int elementCounter;
    private int maxElementNumber;

    public ItemReturner (int max) {
        maxElementNumber = max;
        elementCounter = 0;
    }

    public int getElement() {
        if (elementCounter < maxElementNumber) {
            int curr = elementCounter;
            elementCounter++;
            return curr;
        }
        else return -1;
    }
}
