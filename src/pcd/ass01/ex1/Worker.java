package pcd.ass01.ex1;

public class Worker extends Thread {

	private int currentItemShared;
	private int maxNumberItem;
	private int width;
	private int height;
	private ItemReturner itemReturner;
	private int maxIter;
	private MandelbrotSetImage mandelbrotSetImage;

	public Worker(int w, int h, ItemReturner it, int mIter, MandelbrotSetImage msi){
		width = w;
		height = h;
		itemReturner = it;
		maxNumberItem = width * height;
		maxIter = mIter;
		mandelbrotSetImage = msi;
	}
	
	public void run() {
		concurrentCompute();
	}

	public void concurrentCompute() {
		int currentItemLocal;
		int x, y;
		while (true) {
			synchronized (itemReturner) {
				currentItemLocal = itemReturner.getElement();
			}
			if (currentItemLocal < 0) {
				break;
			}
			else {
				y = currentItemLocal / width;
				x = currentItemLocal % width;
				Complex c = mandelbrotSetImage.getPoint(x, y);
				double level = mandelbrotSetImage.computeColor(c, maxIter);
				int color = (int) (level * 255);
				mandelbrotSetImage.setPixel(y * width + x, color);
			}
		}
	}

	private void log(String msg){
		System.out.println("[WORKER] "+msg);
	}

}
